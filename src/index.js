'use strict';

module.exports = {
    UITreeSelect: require('./component/DropdownTreeSelect'),
    UIInputTreeSelect: require('./component/InputTreeSelect'),
    UIInputTableSelect: require('./component/inputTableSelect')
};