import { Component, UIInput } from 'rainbowui-desktop-core'
import PropTypes from 'prop-types'
import cn from 'classnames/bind'
// import debounce from 'lodash/debounce'
import Tag from '../tag'
import styles from '../../css/input.css'

const cx = cn.bind(styles)

export default class TreeInput extends UIInput {

    constructor(props) {
        super(props)
        // this.delayedCallback = _.debounce((e) => {
        //     this.props.onInputChange(e.target.value)
        // }, 100, {
        //         leading: true
        //     })
        this.isOnComposition = false
        this.isChrome = !!window.chrome && !!window.chrome.webstore
    }

    // onInputChange = (e) => {
    //     e.persist()
    //     this.delayedCallback(e)
    // }

    //获取节点内容，这里的方法几个地方共用的，可能需要通过其他的属性来设置
    getTags(tags = [], onDelete, mostVisableItem) {
        let mostItemCount = 999999999
        if (mostVisableItem > 0) {
            mostItemCount = mostVisableItem
        }
        let { labellabel } = this.props
        return tags.map(
            (tag, i) => {
                if (tag["hideLabel"]) {
                    mostItemCount += 1
                    return null
                }
                if (i >= mostItemCount) {
                    return null
                }
                let { _id, tagClassName } = tag

                let label = tag[labellabel]
                if (tag["wholeLabel"]) {
                    label = tag["wholeLabel"]
                }
                if (label.length > this.props.perItemMaxLength) {
                    label = label.substr(0, this.props.perItemMaxLength) + "..."
                }
                return (
                    <li className={cx('tag-item', tagClassName)} key={`tag-${i}`}>
                        <Tag
                            label={label}
                            id={_id}
                            readOnly={this.props.readOnly}
                            onDelete={onDelete}
                        />
                    </li>
                )
            }
        )
    }

    getTagsOut(tags = [], onDelete, mostVisableItem) {
        let mostItemCount = 999999999
        if (mostVisableItem > 0) {
            mostItemCount = mostVisableItem
        }
        let { labellabel } = this.props;
        let tagLen = tags.length - 1 < 0 ? 0 : tags.length - 1;
        return tags.map(
            (tag, i) => {
                let { _id, tagClassName } = tag

                let label = tag[labellabel]
                if (tag["wholeLabel"]) {
                    label = tag["wholeLabel"]
                }
                return (
                    <span className="outPutText" key={`tag-${i}`}>
                        {label}{i == tagLen?'':','}
                    </span>
                )
            }
        )
    }

    getTagsOutTitle(tags = [], onDelete, mostVisableItem) {
        let mostItemCount = 999999999
        if (mostVisableItem > 0) {
            mostItemCount = mostVisableItem
        }
        let { labellabel } = this.props;
        let result = [];
         tags.map(
            (tag, i) => {
                let { _id, tagClassName } = tag

                let label = tag[labellabel]
                if (tag["wholeLabel"]) {
                    label = tag["wholeLabel"]
                }
                result.push(label)
            }
        )
        return result.join('\n');
    }

    handleComposition(e) {
        if (e.type === 'compositionend') {
            // composition is end
            this.isOnComposition = false

            if (e.target instanceof HTMLInputElement && !this.isOnComposition && this.isChrome) {
                // fire onChange
                this.props.onInputChange(e.target.value)
            }
        } else {
            // in composition
            this.isOnComposition = true
        }
    }

    handleChange(e) {
        // only when onComposition===false to fire onChange
        if (e.target instanceof HTMLInputElement && !this.isOnComposition) {
            this.props.onInputChange(e.target.value)
        }
    }


    render() {
        let uiClass = 'tag-list'
        if (this.props.tags) {
            if (this.props.tags.length > this.props.mostVisableItem) {
                uiClass += ' more'
            }
        }
        else {
            return null
        }
        let placeText = this.props.placeholderText || 'Search';
        if (this.getTags(this.props.tags, this.props.onTagRemove, this.props.mostVisableItem).length > 0) {
            placeText = ''
        }
        return (
            // <ul className={cx(uiClass)}>
            //     {this.getTags(this.props.tags, this.props.onTagRemove, this.props.mostVisableItem)}
            // </ul>
            this.props.io == 'in' ?
                <ul className={cx(uiClass)} >
                    {
                        this.props.disabled ? null :
                        <input id={this.props.pContainerId + '-searchInput'} type='text'
                            ref={this.props.inputRef}
                            placeholder={placeText}
                            onChange={this.handleChange.bind(this)}
                            onFocus={this.props.onFocus}
                            onBlur={this.props.onBlur}
                            onCopy={this.props.onCopy}
                            onCompositionStart={this.handleComposition.bind(this)}
                            onCompositionUpdate={this.handleComposition.bind(this)}
                            onCompositionEnd={this.handleComposition.bind(this)}
                        />
                    }
                    <div>
                        {this.getTags(this.props.tags, this.props.onTagRemove, this.props.mostVisableItem)}
                    </div>
            </ul> :
            <div style={{textOverflow: (this.props.needellipsis ? 'ellipsis' : ''), overflow: this.props.needellipsis ? 'hidden' : '' }} title={this.getTagsOutTitle(this.props.tags, this.props.onTagRemove, this.props.mostVisableItem)}>
                 {this.getTagsOut(this.props.tags, this.props.onTagRemove, this.props.mostVisableItem)}
            </div>

        )
    }
}



/**
 * TwoText component prop types
 */
TreeInput.propTypes = $.extend({}, UIInput.propTypes, {
    readOnly: PropTypes.bool,
    tags: PropTypes.array,
    value: PropTypes.string,
    placeholderText: PropTypes.string,
    onInputChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onCopy: PropTypes.func,
    onTagRemove: PropTypes.func,
    inputRef: PropTypes.func,
    mostVisableItem: PropTypes.number,
    copySplitFlag: PropTypes.string,
    perItemMaxLength: PropTypes.number,
    labellabel: PropTypes.string,
    pContainerId: PropTypes.string

});

/**
 * Get Input component default props
 */
TreeInput.defaultProps = $.extend({}, UIInput.defaultProps, {
    readOnly: false,
});