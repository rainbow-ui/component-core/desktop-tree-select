'use strict';

module.exports = {
    Input: require('./Input'),
    TreeInput: require('./InputIndex'),
    TableInput: require('./inputTable')
};