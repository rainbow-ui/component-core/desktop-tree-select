import { Component, UIInput } from 'rainbowui-desktop-core'
import PropTypes from 'prop-types'
import cn from 'classnames/bind'
// import debounce from 'lodash/debounce'
import Tag from '../tag'
import styles from '../../css/input.css'

const cx = cn.bind(styles)

export default class TableInput extends UIInput {

    constructor(props) {
        super(props);
        this.isOnComposition = false;
        this.isChrome = !!window.chrome && !!window.chrome.webstore
    }

    //获取节点内容，这里的方法几个地方共用的，可能需要通过其他的属性来设置
    getTags(tags = [], onDelete, mostVisableItem) {
        let mostItemCount = 999999999
        if (mostVisableItem > 0) {
            mostItemCount = mostVisableItem
        }
        let { tableLabel, tableId } = this.props;
        let self = this;
        return tags.map(
            (tag, i) => {
                if (tag["hideLabel"]) {
                    mostItemCount += 1
                    return null
                }
                if (i >= mostItemCount) {
                    return null
                }
                let { tagClassName } = tag
                let tableIdValue = tag[tableId]

                let label = tag[tableLabel]
                if (label && label.length > this.props.perItemMaxLength) {
                    label = label.substr(0, this.props.perItemMaxLength) + "..."
                }
                return (
                    <li className={cx('tag-item', tagClassName)} key={`tag-${i}`}>
                        <Tag
                            label={label}
                            id={tableIdValue}
                            onDelete={onDelete}
                            tagObj={tag}
                            pContainerId={this.props.pContainerId}
                        />
                    </li>
                )
            }
        )
    }


    handleComposition(e) {
        if (e.type === 'compositionend') {
            // composition is end
            this.isOnComposition = false

            if (e.target instanceof HTMLInputElement && !this.isOnComposition && this.isChrome) {
                // fire onChange
                this.props.onInputChange(e.target.value)
            }
        } else {
            // in composition
            this.isOnComposition = true
        }
    }

    handleChange(e) {
        // only when onComposition===false to fire onChange
        if (e.target instanceof HTMLInputElement && !this.isOnComposition) {
            this.props.onInputChange(e.target.value)
        }
    }


    render() {
        let uiClass = 'tag-list'
        if (this.props.tags) {
            if (this.props.tags.length > this.props.mostVisableItem) {
                uiClass += ' more'
            }
        }
        else {
            return null
        }
        
        return (
            <ul className={cx(uiClass)} >
                {
                    this.props.disabled ? null :
                    this.props.tags && this.props.tags.length > 0 ?
                    <input id={this.props.pContainerId + '-searchInput'} type='text'
                    ref={this.props.inputRef}
                    onChange={this.handleChange.bind(this)}
                    onFocus={this.props.onFocus}
                    onBlur={this.props.onBlur}
                    onCopy={this.props.onCopy}
                    // onCompositionStart={this.handleComposition.bind(this)}
                    // onCompositionUpdate={this.handleComposition.bind(this)}
                    // onCompositionEnd={this.handleComposition.bind(this)}
                />:
                <input id={this.props.pContainerId + '-searchInput'} type='text'
                    ref={this.props.inputRef}
                    placeholder={this.props.placeholderText || 'Search'}
                    onChange={this.handleChange.bind(this)}
                    onFocus={this.props.onFocus}
                    onBlur={this.props.onBlur}
                    onCopy={this.props.onCopy}
                    // onCompositionStart={this.handleComposition.bind(this)}
                    // onCompositionUpdate={this.handleComposition.bind(this)}
                    // onCompositionEnd={this.handleComposition.bind(this)}
                />
                }
                <div>
                    {this.getTags(this.props.tags, this.props.onTagRemove, this.props.mostVisableItem)}
                </div>
                
            </ul>

        )
    }
}



/**
 * TableInput component prop types
 */
TableInput.propTypes = $.extend({}, UIInput.propTypes, {
    tags: PropTypes.array,
    value: PropTypes.string,
    placeholderText: PropTypes.string,
    onInputChange: PropTypes.func,
    onFocus: PropTypes.func,
    onBlur: PropTypes.func,
    onCopy: PropTypes.func,
    onTagRemove: PropTypes.func,
    inputRef: PropTypes.func,
    mostVisableItem: PropTypes.number,
    copySplitFlag: PropTypes.string,
    perItemMaxLength: PropTypes.number,
    tableLabel: PropTypes.string,
    tableId: PropTypes.string,
    pContainerId: PropTypes.string

});

/**
 * Get TableInput component default props
 */
TableInput.defaultProps = $.extend({}, UIInput.defaultProps, {
});