/*!
 * React Dropdown Tree Select
 * A lightweight, fast and highly customizable tree select component.
 * Hrusikesh Panda <hrusikesh.panda@dowjones.com>
 * Copyright (c) 2017 Dow Jones, Inc. <support@dowjones.com> (http://dowjones.com)
 * license MIT
 * see https://github.com/dowjones/react-dropdow-tree-select
 */
import { UIInput, UISwitch, Param, UIMessageHelper, OnChangeEvent, UIChoose } from 'rainbowui-desktop-core'
import PropTypes, { instanceOf } from 'prop-types'
import cn from 'classnames/bind'
import Dropdown from './Dropdown'
import DropdownTrigger from './DropdownTrigger'
import DropdownContent from './DropdownContent'
import { Util } from 'rainbow-desktop-tools';
import TreeManager from './tree-manager'
import Tree from './tree'
import { TreeInput } from './input'
import { SelectUtil } from './select-utils.js'
import styles from '../css/index.css'

const cx = cn.bind(styles)

export default class InputTreeSelect extends UIInput {
    constructor(props) {
        super(props);
        this.state = {
            dropdownActive: this.props.showDropdown || false,
            searchModeOn: false,
            inputLiClass: 'inputClass',
            switchState: { switchValue: '' },
            componentValue: '',
            tree: null,
            selectValue: [],

            chooseValueList: [props.allText, props.selectedText],
            chooseValue: props.allText
        }

        if (this.props.io == "out") {
            this.props.readOnly = true
        }

        this.onInputChange = this.onInputChange.bind(this)
        this.onDrowdownHide = this.onDrowdownHide.bind(this)
        if (!Util.parseBool(this.props.readOnly)) {
            this.onCheckboxChange = this.onCheckboxChange.bind(this)
            this.onTagRemove = this.onTagRemove.bind(this)
        }
        this.notifyChange = this.notifyChange.bind(this)
        this.onNodeToggle = this.onNodeToggle.bind(this)
        // this.searchInput =  {}
    }

    notifyChange(...args) {
        var tempArgs = [...args];
        typeof this.props.onChange === 'function' && this.props.onChange(...args)
        if (this.props.model && this.props.property) {
            let propertyArr = this.props.model[this.props.property]
            if (propertyArr) {
                if (propertyArr instanceof Array) {
                    if (propertyArr.length > 0) {
                        $('#' + this.componentId).val('hasValue')
                    }
                    else {
                        $('#' + this.componentId).val('')
                    }
                }
            }
        }
    }

    createList(tree) {
        this.treeManager = new TreeManager(tree, this.props.jsonConfig, this.props.pinyinForSearch)
        return this.treeManager.tree
    }

    resetSearch = () => {
        // restore the tree to its pre-search state
        this.setState({ tree: this.treeManager.restoreNodes(), searchModeOn: false, allNodesHidden: false })
        // clear the search criteria and avoid react controlled/uncontrolled warning
        if (this.searchInput) {
            this.searchInput.value = ''
        }
    }

    hashcode(str) {
        var hash = 0, i, chr, len;
        if (str.length === 0) return hash;
        for (i = 0, len = str.length; i < len; i++) {
            chr = str.charCodeAt(i);
            hash = ((hash << 5) - hash) + chr;
            hash |= 0; // Convert to 32bit integer
        }
        return hash;
    }

    compareObjects(objA, objB) {
        return this.hashcode(JSON.stringify(objA)) == this.hashcode(JSON.stringify(objB))
    }

    componentDidMount() {

        SelectUtil.setSelectComponentValue(this.state.tags.length, this.componentId);
        if (this.props.io != "out") {
            this.initValidator();
        }
    }

    initSort(value) {
        let _self = this;
        // let { sortColumn, sortWay } = this.state;

        let sorter = null;

        // if no sorter specific in column
        sorter = sorter == null ? (function (a, b) {
            let v1 = eval("a.label");
            let v2 = eval("b.label");
            return _self.compareFunc(v1, v2);
        }) : sorter;

        value.sort(sorter);

        return value;
    }

    compareFunc(param1, param2) {
        let _param1 = Number(param1);
        let _param2 = Number(param2);
        if (!isNaN(_param1)) {
            param1 = _param1;
        }
        if (!isNaN(_param2)) {
            param2 = _param2;
        }
        //如果两个参数均为字符串类型
        if (typeof param1 == "string" && typeof param2 == "string") {
            return param1.localeCompare(param2, 'zh');
        }
        //如果参数1为数字，参数2为字符串
        if (typeof param1 == "number" && typeof param2 == "string") {
            return -1;
        }
        //如果参数1为字符串，参数2为数字
        if (typeof param1 == "string" && typeof param2 == "number") {
            return 1;
        }
        //如果两个参数均为数字
        if (typeof param1 == "number" && typeof param2 == "number") {
            if (param1 > param2) return 1;
            if (param1 == param2) return 0;
            if (param1 < param2) return -1;
        }
    }

    sortData(data) {
        for (let i = 0; i < data.length; i++) {
            //guf： 这里报错，不过应该重新放开
            this.initSort(data[i].children)
        }
    }

    componentWillMount() {
        super.componentWillMount();

        let msg = ""
        if (!this.props.data) {
            this.props.data = []
        }

        if (this.props.data && Util.parseBool(this.props.enableSort)) {
            this.sortData(this.props.data);
        }
        const tree = this.createList(this.props.data)
        if (tree.size) {
            if (this.props.defaultValue) {
                if (this.props.model[this.props.property] instanceof Array) {
                    this.props.defaultValue.forEach(element => {
                        this.props.model[this.props.property].push(element)
                    });
                }
            }
        }
        let bindModel = this.props.model
        let bindProperty = this.props.property
        let checkNodeIds = []
        if (bindModel && bindProperty) {

            //todo:  这里需要根据对象或者ID来修改
            //点:接收参数的时候也要
            //考虑jsonConfig的内容变动

            checkNodeIds = bindModel[bindProperty]
            if (checkNodeIds && tree.size > 0) {
                msg = this.treeManager.setNodeCheckedStates(checkNodeIds, true, this.props.noCheckParent)
            }
            const tags = this.treeManager.getTags(null, this.state.inputLiClass, this.props.noCheckParent, this.props.showParentSplitSign)

            this.setState({ tree, tags })
        }
    }


    componentDidUpdate() {
        if (!Util.parseBool(this.props.readOnly)) {
            this.onCheckboxChange = this.onCheckboxChange.bind(this)
            this.onTagRemove = this.onTagRemove.bind(this)
        }
    }

    componentWillReceiveProps(nextProps) {
        let bindProperty = nextProps.property
        let msg = ""
        if (nextProps.io == "out") {
            nextProps.readOnly = true
        }

        if (!this.props.data) {
            this.props.data = []
        }

        if (nextProps.model && nextProps.property && this.props.model && this.props.property && this.props.data && nextProps.data) {
            if (this.compareObjects(this.props.data, nextProps.data) && this.compareObjects(this.props.model, nextProps.model) && this.compareObjects(this.props.property, nextProps.property) && !Util.parseBool(this.props.needCompare)) {
                return
            }
        }

        if (!Util.parseBool(nextProps.readOnly)) {
            this.onCheckboxChange = this.onCheckboxChange.bind(this)
            this.onTagRemove = this.onTagRemove.bind(this)
            // this.onDrowdownHide = this.onDrowdownHide.bind(this)
        }
        if (Util.parseBool(this.props.enableSort)) {
            this.sortData(nextProps.data);
        }
        const tree = this.createList(nextProps.data)
        let bindModel = nextProps.model
        let checkNodeIds = []
        if (bindModel && bindProperty) {
            checkNodeIds = bindModel[bindProperty]
            if (checkNodeIds && tree.size > 0) {
                msg = this.treeManager.setNodeCheckedStates(checkNodeIds, true, this.props.noCheckParent)
            }
        }
        const tags = this.treeManager.getTags(null, this.state.inputLiClass, this.props.noCheckParent, this.props.showParentSplitSign)
        SelectUtil.setSelectComponentValue(tags.length, this.componentId)
        this.setState({ tree, tags })
    }


    onDrowdownHide() {
        if ($('#' + this.componentId).parents('div [needreposition=1]') && $('#' + this.componentId).parents('div [needreposition=1]').length > 0) {

            // $($('#' + this.componentId).parents('div [needreposition=1]')[0]).css({ position: 'relative', left: '0px', marginTop: '0px' });
        }

        // needed when you click an item in tree and then click back in the input box.
        // react-simple-dropdown behavior is toggle since its single select only
        // but we want the drawer to remain open in this scenario as we support multi select
        if (this.keepDropdownActive) {
            this.dropdown.show()
        } else {
            this.resetSearch()
        }
    }

    contentOnShow() {
        if ($('#' + this.componentId).parents('td') && $('#' + this.componentId).parents('td').length > 0) {
            // let l = $('#for-input-' + this.componentId).position().left;
            // let w = $('#' + this.componentId).parents('td').attr('realwidth');
            let obj = $("#dropdown-trigger-" + this.componentId);
            let l = obj.position().left;
            let w = obj.width();
            // let curObjH = $('#for-input-' + this.componentId).height() / 2;
            // $('#for-input-' + this.componentId).css({ position: 'absolute', left: l + 'px', marginTop: '-' + curObjH + 'px' });
            $("#dropdown-content-" + this.componentId).css({ left: l, width: w });
        }

        this.setState(
            {
                switchState: { switchValue: 'N' }
            }
        )
        $("#" + this.componentId + "-searchInput").focus()
    }

    onInputChange(value) {
        let self = this
        let treeSearchMode = Util.parseBool(this.props.treeSearchMode)
        const { allNodesHidden, tree } = this.treeManager.filterTree(value, treeSearchMode, this.props.jsonConfig.label, this.props.pinyinForSearch)
        let searchModeOn = value.length > 0
        if (treeSearchMode) {
            searchModeOn = false
        }
        // this.setStateAsync(tree, searchModeOn, allNodesHidden, value)
        this.setStatePromise({ tree, searchModeOn, allNodesHidden }).then(() => {
            $.each($('#' + this.componentId + ' .dropdown-content > div ul > li > label >span'), (index, obj) => {
                self.highlight(obj, value)
            })
        })


    }
    setStatePromise(state) {
        return new Promise((resolve) => {
            this.setState(state, resolve)
        });
    }

    async setStateASync(tree, searchModeOn, allNodesHidden, value) {
        await this.setState({
            tree, searchModeOn, allNodesHidden
        })

        $.each($('#' + this.componentId + ' .dropdown-content > div ul > li > label >span'), (index, obj) => {
            this.highlight(obj, value)
        })
    }

    onTagRemove(id) {
        this.onCheckboxChange(null, id, false)
    }

    getSelfById(data, id) {
        let flag = false;
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id) {
                if (data[i].hasChild == 1 && (!data[i].children || (data[i].children && data[i].children.length) == 0)) {
                    //说明有儿子，但是还没加载
                    return true;
                } else {
                    return false;
                }
            } else if (data[i].children && data[i].children.length > 0) {
                flag = this.getSelfById(data[i].children, id);
                if (flag) {
                    //说明找到自己了，并且有儿子但还没加载，直接退出去
                    break;
                }
            }

        }
        return flag;
    }

    //得到儿子之后，塞到数据中
    setChild(data, id, child) {
        let flag = false;
        for (let i = 0; i < data.length; i++) {
            if (data[i].id == id) {
                data[i].children = child;
                return true;
            } else if (data[i].children && data[i].children.length > 0) {
                flag = this.setChild(data[i].children, id, child);
                if (flag) {
                    break;
                }
            }
        }
        return flag;
    }

    getArrayAttr(arr, attrName) {
        let arrAtt = [];
        arr.forEach((item) => {
            arrAtt.push(item[attrName])
        })
        return arrAtt;
    }


    setParentExpanded(data, node) {
        if (data) {
            for (let i = 0; i < data.length; i++) {
                let nodeParent = this.treeManager.getNodeById(node.parent)
                if (nodeParent) {
                    nodeParent.expanded = true;
                    this.setParentExpanded(data[i].children, nodeParent);
                }
            }
        }
    }

    onNodeToggle(node) {
        if (Util.parseBool(this.props.needGetChildFromAjax)) {
            let _self = this;
            if (this.getSelfById(this.props.data, node._id)) {
                let newData = this.props.data; //_.cloneDeep(this.props.data);

                let parentId = { parent: node._id };
                AjaxUtil.call(this.props.url, parentId, { "method": "POST" }).then((result) => {
                    //得到了儿子，需要塞到数据中
                    _self.setChild(newData, node._id, result);

                    /**** 直接用数据可以，但导致收起来有问题 */
                    const tree = _self.createList(newData);

                    // let nodeParent = this.treeManager.getNodeById(node.parent)
                    _self.setParentExpanded(newData, node)
                    // nodeParent.expanded = true;
                    // node.expanded = true
                    this.treeManager.tree = tree
                    _self.treeManager.toggleNodeExpandState(node.id)
                    _self.setState({ tree: this.treeManager.tree })
                    /**** 直接用数据end */
                });

            }
        }
        if (!this.getSelfById(this.props.data, node._id)) {
            if (typeof this.props.onNodeToggle === 'function') {
                let node = this.treeManager.getNodeById(id)
                let cNodes = this.props.onNodeToggle(node)
                if (!node._children) {
                    let newTreeMap = new Map()

                    let codeNodeMapList = this.walkNodes({ nodes: cNodes, parent: node, depth: parent._depth + 1 }, this.props.jsonConfig)
                    this.treeManager.tree.forEach((node, key) => {


                        newTreeMap.set(key, node)
                        if (key == id) {
                            codeNodeMapList.forEach(function (cnode, ckey) {
                                newTreeMap.set(ckey, cnode)
                            }, this);
                        }
                    })
                    this.treeManager.tree = newTreeMap
                }
            }


            this.treeManager.toggleNodeExpandState(node._id)
            this.setState({ tree: this.treeManager.tree })

        }
    }



    onCheckboxChange(event, id, checked) {
        let ids = []
        let nodes = []
        ids.push(id)
        let msg = ""
        if (!Util.parseBool(this.props.readOnly)) {
            this.treeManager.getNodeIdsByIdRecursive(ids, nodes)
            let curNode = this.treeManager.getNodeById(id)
            if (this.props.displayByOrder) {
                let newSelectValue = []
                if (checked) {
                    this.state.selectValue.push(curNode);
                } else {
                    for (let i = 0; i < this.state.selectValue.length; i++) {
                        if (this.state.selectValue[i] != curNode && this.state.selectValue.length > 1) {
                            newSelectValue.push(this.state.selectValue[i]);
                        }
                    }
                    /* 改选两个以上元素点击X按钮会删除多个 */
                    this.state.selectValue = newSelectValue
                }
            }
            let breakFunction = false
            let tempNodeIds = []
            const tagsNode = this.treeManager.getTags(tempNodeIds, this.state.inputLiClass, this.props.noCheckParent, this.props.showParentSplitSign)

            if (this.props.onCheckboxChange) {
                if (this.props.noCheckParent) {
                    breakFunction = this.props.onCheckboxChange(event, [curNode[this.props.jsonConfig["id"]]], checked, curNode, tagsNode)
                }
                else {
                    breakFunction = this.props.onCheckboxChange(event, nodes, checked, curNode, tagsNode)
                }
            }

            if (!breakFunction) {
                msg = this.treeManager.setNodeCheckedState(id, checked, this.props.noCheckParent, this.props.singleSelect)
                if (msg) {
                    UIMessageHelper.warning(msg)
                    return
                }

                let nodeIds = []
                const tags = this.treeManager.getTags(nodeIds, this.state.inputLiClass, this.props.noCheckParent, this.props.showParentSplitSign)
                SelectUtil.setSelectComponentValue(tags.length, this.componentId)
                // let checkedArr = []
                // this.treeManager.getNodeIdsByIdRecursive(nodeIds, checkedArr)
                if (this.props.displayByOrder) {
                    let newTags = this.state.selectValue; // 单选时出问题
                    this.setState({ tree: this.treeManager.tree, tags })
                } else {
                    this.setState({ tree: this.treeManager.tree, tags })
                }
                let checkedLeafNodeIds = []
                if (this.props.model && this.props.property) {
                    if (this.props.propSaveConentType == "id") {

                        this.treeManager.getNodeIdsByIdRecursive(nodeIds, checkedLeafNodeIds)
                        if (this.props.propSaveConent == "leaf") {

                            this.props.model[this.props.property] = checkedLeafNodeIds
                        }
                        else {
                            let checkedTagsNodeIds = []
                            let idSign = this.props.jsonConfig["id"]
                            tags.forEach(element => {
                                checkedTagsNodeIds.push(element[idSign])
                            });

                            this.props.model[this.props.property] = checkedTagsNodeIds
                        }
                    } else {

                        this.treeManager.getNodeIdsByIdRecursive(nodeIds, checkedLeafNodeIds, true)
                        if (this.props.propSaveConent == "leaf") {
                            this.props.model[this.props.property] = checkedLeafNodeIds
                        }
                        else {
                            this.props.model[this.props.property] = tags
                        }
                    }

                }
                this.notifyChange(curNode, checkedLeafNodeIds, tags)
            } else if (breakFunction && this.props.displayByOrder) {
                let nodeIds = []
                const tags = this.treeManager.getTags(nodeIds, this.state.inputLiClass, this.props.noCheckParent, this.props.showParentSplitSign)
                if (this.state.selectValue.length != tags.length) {
                    this.state.selectValue = this.state.selectValue.slice(0, tags.length)

                }
                let newTags = this.state.selectValue;
                this.setState({ tree: this.treeManager.tree, tags: newTags })
            }
        }
    }

    onAction = (actionId, nodeId) => {
        // typeof this.props.onAction === 'function' && this.props.onAction(actionId, this.treeManager.getNodeById(nodeId))
        console.log("onAction")
    }

    copyToClipboard(text) {
        // IE specific
        if (window.clipboardData && window.clipboardData.setData) {
            return clipboardData.setData("Text", text);
        }

        // all other modern
        let target = document.createElement("textarea");
        target.style.position = "absolute";
        target.style.left = "-9999px";
        target.style.top = "0";
        target.textContent = text;
        document.body.appendChild(target);
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection of fall back to prompt
        try {
            document.execCommand("copy");
            target.remove();
        } catch (e) {
            console.log("Can't copy string on this browser. Try to use Chrome, Firefox or Opera.")
            window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
        }
    }

    copyLabel(event, value) {
        let label = this.props.jsonConfig.label
        event.preventDefault()
        let result = []
        this.state.tags.forEach(element => {
            result.push(element[label])
        });
        this.copyToClipboard(result.join(this.props.copySplitFlag))

    }

    onSwitchChanged(event) {
        if (this.state.switchState.switchValue == 'Y') {
            const { allNodesHidden, tree } = this.treeManager.filterTreeChecked(true)
            let searchModeOn = false
            this.setState({ tree, searchModeOn, allNodesHidden })
        } else {
            let treeSearchMode = Util.parseBool(this.props.treeSearchMode)
            const { allNodesHidden, tree } = this.treeManager.filterTree('', treeSearchMode, this.props.jsonConfig.label, this.props.pinyinForSearch)
            let searchModeOn = false
            this.setState({ tree, searchModeOn, allNodesHidden })
        }
    }

    onChooseChanged() {
        let { chooseValue, chooseValueList } = this.state;
        if (chooseValue === chooseValueList[0]) {
            let treeSearchMode = Util.parseBool(this.props.treeSearchMode)
            const { allNodesHidden, tree } = this.treeManager.filterTree('', treeSearchMode, this.props.jsonConfig.label, this.props.pinyinForSearch)
            let searchModeOn = false
            this.setState({ tree, searchModeOn, allNodesHidden })
        } else {
            const { allNodesHidden, tree } = this.treeManager.filterTreeChecked(true)
            let searchModeOn = false;
            this.setState({ tree, searchModeOn, allNodesHidden })
        }
    }

    highlight(ele, keyword) {
        var textbox = ele;
        var regexClear = /(<([^>]+)>)/ig
        var temp = textbox.innerHTML.replace(regexClear, '');

        if ("" == keyword) {
            textbox.innerHTML = temp
            return;
        }

        let pinyinIndex = $(ele).attr("pinyin")
        //获取所有文字内容，并清空之前被高亮的标签
        var htmlReg = new RegExp("\<.*?\>", "i");
        var arr = new Array();

        //替换HTML标签 
        for (var i = 0; true; i++) {
            //匹配html标签 
            var tag = htmlReg.exec(temp);
            if (tag) {
                arr[i] = tag;
            } else {
                break;
            }
            temp = temp.replace(tag, "{[(" + i + ")]}");
        }

        if (pinyinIndex >= 0) {
            let highlightWords = temp.substr(pinyinIndex, keyword.length)
            temp = temp.replace(highlightWords, "<b style='color:Red;'>" + highlightWords + "</b>")
        }
        textbox.innerHTML = temp;
    }

    renderInputComponent() {
        let { jsonConfig } = this.props
        let triggerStyle = {}
        if (this.props.inputWidth) {
            triggerStyle.width = this.props.inputWidth
        }
        else {
            triggerStyle.width = "100%"
        }
        if (this.props.io == "out") {
            triggerStyle.width = "100%"
            triggerStyle.border = "none"
        }
        return (
            this.props.io == 'in' ?
                <div className={cn(this.props.className, 'react-dropdown-tree-select', 'input-group')} disabled={this.props.disabled} id={this.componentId} name={this.getName()} required={this.props.required}>
                    <Dropdown disabled={this.props.disabled} ref={el => { this.dropdown = el }} enabledDrop={this.props.enabledDrop} onHide={this.onDrowdownHide} onShow={this.contentOnShow.bind(this)}>
                        <DropdownTrigger
                            id={"dropdown-trigger-" + this.componentId}
                            className={cx('dropdown-trigger')}
                            style={triggerStyle}
                            onMouseOver={this.showClearBtn.bind(this)}
                            onMouseOut={this.hideClearBtn.bind(this)}
                        >
                            <TreeInput
                                inputRef={el => { this.searchInput = el }}
                                disabled={this.props.disabled}
                                tags={this.state.tags}
                                placeholderText={this.props.placeholderText}
                                onInputChange={this.onInputChange}
                                onFocus={() => { this.keepDropdownActive = true }}
                                onBlur={() => { this.keepDropdownActive = false }}
                                onTagRemove={this.onTagRemove}
                                mostVisableItem={this.props.mostVisableItem}
                                perItemMaxLength={this.props.perItemMaxLength}
                                labellabel={jsonConfig.label}
                                onCopy={this.copyLabel.bind(this)}
                                pContainerId={this.componentId}
                                readOnly={this.props.readOnly}
                                io={this.props.io}
                            />
                            {this.props.showDeleteIcon ?
                                <span className="rainbow Clear rb-clear" style={{
                                    display: `${this.state.isShowClear ? 'block' : 'none'}`
                                }} onClick={() => this.clearAll()}></span> : null}
                            <span className="rainbow SingArrowDown"></span>
                        </DropdownTrigger>
                        <DropdownContent
                            id={"dropdown-content-" + this.componentId}
                            className={cx('dropdown-content')}
                            style={this.props.contentWidth ? { width: this.props.contentWidth } : { "width": "100%" }}>
                            <div className={`${this.props.singleSelect ? 'tree-select-single' : ''}`}>
                                {/* <UISwitch model={this.state.switchState} property="switchValue" onText="已选" offText="所有" onChange={this.onSwitchChanged.bind(this)} /> */}
                                {this.props.singleSelect ? null : this.props.isShowSwitch ?
                                    <UIChoose value={this.state.chooseValueList} multiSelect="false" model={this.state}
                                        property="chooseValue" onClick={this.onChooseChanged.bind(this)} /> : null}
                                {this.state.allNodesHidden
                                    ? <span className='no-matches'>No matches found</span>
                                    : (
                                        <Tree data={this.state.tree}
                                            searchModeOn={this.state.searchModeOn}
                                            onAction={this.onAction}

                                            onCheckboxChange={this.onCheckboxChange.bind(this)}
                                            onNodeToggle={this.onNodeToggle} jsonConfig={jsonConfig} />
                                    )
                                }
                            </div>

                        </DropdownContent>
                    </Dropdown>
                </div>
                :
                <TreeInput
                    inputRef={el => { this.searchInput = el }}
                    disabled={this.props.disabled}
                    tags={this.state.tags}
                    placeholderText={this.props.placeholderText}
                    onInputChange={this.onInputChange}
                    onFocus={() => { this.keepDropdownActive = true }}
                    onBlur={() => { this.keepDropdownActive = false }}
                    onTagRemove={this.onTagRemove}
                    mostVisableItem={this.props.mostVisableItem}
                    perItemMaxLength={this.props.perItemMaxLength}
                    labellabel={jsonConfig.label}
                    onCopy={this.copyLabel.bind(this)}
                    pContainerId={this.componentId}
                    io={this.props.io}
                    needellipsis={this.props.needellipsis}
                />
        )
    }

    showClearBtn() {
        if (this.state.tags.length > 0) {
            this.setState({ isShowClear: true });
        }
    }

    hideClearBtn() {
        this.setState({ isShowClear: false });
    }

    // 删除按钮
    clearAll() {
        let { tags = [] } = this.state;
        tags.map(item => {
            this.onCheckboxChange(null, item.elementId, false);
        })
    }


    walkNodes({ nodes, list = new Map(), parent, depth = 0 }, config) {
        nodes.forEach((node, i) => {
            node._depth = depth
            if (parent) {
                node._id = node[config.id] || `${parent._id}-${i}`
                node._parent = parent._id
                if (!parent._children) {
                    parent._children = []
                }
                parent._children.push(node._id)
            } else {
                node._id = node[config.id] || `${i}`
            }

            list.set(node._id, node)
            if (node[config.children]) {
                node._children = []
                this.walkNodes({ nodes: node[config.children], list, parent: node, depth: depth + 1 }, config)
                delete node.children
            }
        })
        return list
    }
}



/**
 * InputTreeSelect component prop types
 */
InputTreeSelect.propTypes = $.extend({}, UIInput.propTypes, {
    data: PropTypes.array.isRequired,
    placeholderText: PropTypes.string,
    showDropdown: PropTypes.bool,
    className: PropTypes.string,
    onChange: PropTypes.func,
    onCheckboxChange: PropTypes.func,
    // onAction: PropTypes.func,
    // onNodeToggle: PropTypes.func,
    defaultValue: PropTypes.array,
    readOnly: PropTypes.bool,
    treeSearchMode: PropTypes.bool,
    singleSelect: PropTypes.bool,
    mostVisableItem: PropTypes.number,
    copySplitFlag: PropTypes.string,
    perItemMaxLength: PropTypes.number,
    inputWidth: PropTypes.string,
    contentWidth: PropTypes.string,
    noCheckParent: PropTypes.bool,
    searchHighLight: PropTypes.bool,
    pinyinForSearch: PropTypes.bool,
    showParentSplitSign: PropTypes.string,
    propSaveConent: PropTypes.oneOf(["leaf", "tag"]),
    propSaveConentType: PropTypes.oneOf(["id", "node"]),
    jsonConfig: PropTypes.shape({
        id: PropTypes.string,
        children: PropTypes.string,
        label: PropTypes.string
    }),
    overflowMarginTop: PropTypes.string,
    displayByOrder: PropTypes.bool,
    enabledDrop: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]),
    needellipsis: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    needCompare: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    handlerGetChildEvent: PropTypes.func, // hasChild时获取子节点事件
    needGetChildFromAjax: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]), // 子节点是否动态从接口取, true时，必须提供url
    url: PropTypes.string,  // 获取子节点url
    enableSort: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    allText: PropTypes.string,
    selectedText: PropTypes.string,
    disabled: PropTypes.bool,
    isShowSwitch: PropTypes.bool,
    showDeleteIcon: PropTypes.bool
})

/**
 * Get InputTreeSelect component default props
 */
InputTreeSelect.defaultProps = $.extend({}, UIInput.defaultProps, {
    readOnly: false,
    treeSearchMode: true,
    mostVisableItem: 3,
    copySplitFlag: ",",
    perItemMaxLength: 8,
    defaultValue: [],
    noCheckParent: false,
    propSaveConent: "leaf",
    propSaveConentType: "id",
    searchHighLight: false,
    singleSelect: false,
    pinyinForSearch: true,
    componentType: 'InputTreeSelect',
    jsonConfig: {
        id: "id",
        children: "children",
        label: "label"
    },
    overflowMarginTop: '-14px',
    displayByOrder: false,
    needellipsis: true,
    enabledDrop: false, // 可以展开下拉
    needCompare: false,  // 要不要每次跳过比较
    needGetChildFromAjax: false,
    url: '',
    enableSort: true, // 是否排序
    allText: 'All',
    selectedText: 'Selected',
    disabled: false,
    isShowSwitch: true,
    showDeleteIcon: true
});