/*!
 * React Dropdown Tree Select
 * A lightweight, fast and highly customizable tree select component.
 * Hrusikesh Panda <hrusikesh.panda@dowjones.com>
 * Copyright (c) 2017 Dow Jones, Inc. <support@dowjones.com> (http://dowjones.com)
 * license MIT
 * see https://github.com/dowjones/react-dropdow-tree-select
 */
import { UIInput, UIMessageHelper } from 'rainbowui-desktop-core'
import PropTypes, { instanceOf } from 'prop-types'
import cn from 'classnames/bind'
import Dropdown from './Dropdown'
import DropdownTrigger from './DropdownTrigger'
import DropdownContent from './DropdownContent'
import { Util } from 'rainbow-desktop-tools';
import TreeManager from './tree-manager'
import Tree from './tree'
import { Input } from './input'
import styles from '../css/index.css'
import { SelectUtil } from './select-utils.js'

const cx = cn.bind(styles)

export default class DropdownTreeSelect extends UIInput {


  constructor(props) {
    super(props)



    this.state = { dropdownActive: this.props.showDropdown || false, searchModeOn: false }
    if (this.props.io == "out") {
      this.props.readOnly = true
    }

    this.onInputChange = this.onInputChange.bind(this)
    this.onDrowdownHide = this.onDrowdownHide.bind(this)
    if (!Util.parseBool(this.props.readOnly)) {
      this.onCheckboxChange = this.onCheckboxChange.bind(this)
      this.onTagRemove = this.onTagRemove.bind(this)
    }
    this.notifyChange = this.notifyChange.bind(this)
    this.onNodeToggle = this.onNodeToggle.bind(this)
  }

  notifyChange(...args) {
    this.validatorValuePut(...args)
    typeof this.props.onChange === 'function' && this.props.onChange(...args)
    // typeof this.props.onChange === 'function' && this.props.onChange()

  }


  validatorValuePut(curNode, selectedNodes) {
    $("#" + this.componentId + "_validationGroup").val(selectedNodes.length == 0 ? null : selectedNodes.length)
  }

  createList(tree) {
    this.treeManager = new TreeManager(tree, this.props.jsonConfig, this.props.pinyinForSearch)
    return this.treeManager.tree
  }

  resetSearch = () => {
    // restore the tree to its pre-search state
    this.setState({ tree: this.treeManager.restoreNodes(), searchModeOn: false, allNodesHidden: false })
    // clear the search criteria and avoid react controlled/uncontrolled warning
    this.searchInput.value = ''
  }



  hashcode(str) {
    var hash = 0, i, chr, len;
    if (str.length === 0) return hash;
    for (i = 0, len = str.length; i < len; i++) {
      chr = str.charCodeAt(i);
      hash = ((hash << 5) - hash) + chr;
      hash |= 0; // Convert to 32bit integer
    }
    return hash;
  }

  compareObjects(objA, objB) {
    return this.hashcode(JSON.stringify(objA)) == this.hashcode(JSON.stringify(objB))
  }

  componentDidMount() {
    SelectUtil.setSelectComponentValue(this.state.tags.length, this.componentId)
  }

  componentWillMount() {
    super.componentWillMount();


    let msg = ""
    if (!this.props.data) {
      this.props.data = []
    }

    const tree = this.createList(this.props.data)
    if (tree.size) {
      if (this.props.defaultValue) {
        if (this.props.model[this.props.property] instanceof Array) {
          this.props.defaultValue.forEach(element => {
            this.props.model[this.props.property].push(element)
          });
        }
      }
    }
    let bindModel = this.props.model
    let bindProperty = this.props.property
    let checkNodeIds = []
    if (bindModel && bindProperty) {
      checkNodeIds = bindModel[bindProperty]
      if (checkNodeIds && tree.size > 0) {
        msg = this.treeManager.setNodeCheckedStates(checkNodeIds, true, this.props.noCheckParent)
      }
    }
    const tags = this.treeManager.getTags()

    this.setState({ tree, tags })

    if (msg) {
      UIMessageHelper.warning(msg)
    }
  }


  componentDidUpdate() {
  }

  componentWillReceiveProps(nextProps) {


    let msg = ""
    if (nextProps.io == "out") {
      nextProps.readOnly = true
    }

    if (!nextProps.data) {
      nextProps.data = []
    }
    let bindProperty = nextProps.property

    if (nextProps.model && nextProps.property && this.props.model && this.props.property && this.props.data && nextProps.data) {
      if (this.compareObjects(this.props.data, nextProps.data) && this.compareObjects(this.props.model, nextProps.model) && this.compareObjects(this.props.property, nextProps.property)) {
        return
      }
    }

    if (Util.parseBool(nextProps.readOnly)) {
      this.onCheckboxChange = null
      this.onTagRemove = null
    }
    else {
      this.onCheckboxChange = this.onCheckboxChange.bind(this)
      this.onTagRemove = this.onTagRemove.bind(this)
    }

    const tree = this.createList(nextProps.data)
    let bindModel = nextProps.model
    let checkNodeIds = []
    if (bindModel && bindProperty) {
      checkNodeIds = bindModel[bindProperty]
      if (checkNodeIds && tree.size > 0) {
        msg = this.treeManager.setNodeCheckedStates(checkNodeIds, true, this.props.noCheckParent)
      }
    }
    const tags = this.treeManager.getTags()
    SelectUtil.setSelectComponentValue(tags.length, this.componentId)
    this.setState({ tree, tags })

    if (msg) {
      UIMessageHelper.warning(msg)
    }
  }

  onDrowdownHide() {
    // needed when you click an item in tree and then click back in the input box.
    // react-simple-dropdown behavior is toggle since its single select only
    // but we want the drawer to remain open in this scenario as we support multi select
    if (this.keepDropdownActive) {
      this.dropdown.show()
    } else {
      this.resetSearch()
    }
  }

  onInputChange(event) {
    let value = event.target.value
    let treeSearchMode = Util.parseBool(this.props.treeSearchMode)
    const { allNodesHidden, tree } = this.treeManager.filterTree(value, treeSearchMode, this.props.jsonConfig.label)
    let searchModeOn = value.length > 0
    if (treeSearchMode) {
      searchModeOn = false
    }
    this.setState({ tree, searchModeOn, allNodesHidden })

  }

  onTagRemove(id) {
    this.onCheckboxChange(null, id, false)

  }

  onNodeToggle(node) {
    this.treeManager.toggleNodeExpandState(node._id)/* 组件不能展开子元素问题 */
    this.setState({ tree: this.treeManager.tree })
  }

  onCheckboxChange(event, id, checked) {
    let ids = []
    let nodes = []
    ids.push(id)

    this.treeManager.getNodeIdsByIdRecursive(ids, nodes)
    let curNode = this.treeManager.getNodeById(id)
    let breakFunction = false
    let tempNodeIds = []
    const tagsNode = this.treeManager.getTags(tempNodeIds, this.state.inputLiClass)
    if (this.props.onCheckboxChange) {
      breakFunction = this.props.onCheckboxChange(event, nodes, checked, curNode, tagsNode)
    }

    if (!breakFunction) {
      let msg = this.treeManager.setNodeCheckedState(id, checked, this.props.noCheckParent)
      if (msg) {
        UIMessageHelper.warning(msg)
        return
      }

      let nodeIds = []
      const tags = this.treeManager.getTags(nodeIds)
      SelectUtil.setSelectComponentValue(tags.length, this.componentId)
      // let checkedArr = []
      // this.treeManager.getNodeIdsByIdRecursive(nodeIds, checkedArr)
      this.setState({ tree: this.treeManager.tree, tags })

      let checkedLeafNodeIds = []
      this.treeManager.getNodeIdsByIdRecursive(nodeIds, checkedLeafNodeIds)
      if (this.props.model && this.props.property) {
        if (this.props.propSaveConent == "leaf") {
          this.props.model[this.props.property] = checkedLeafNodeIds
        }
        else {
          tags.forEach(element => {
            console.log(tags)
          });
          // this.props.model[this.props.property] = tags
        }
      }
      this.notifyChange(curNode, checkedLeafNodeIds, tags)
    }
  }

  onAction = (actionId, nodeId) => {
    // typeof this.props.onAction === 'function' && this.props.onAction(actionId, this.treeManager.getNodeById(nodeId))
    console.log("onAction")
  }




  copyToClipboard(text) {
    // IE specific
    if (window.clipboardData && window.clipboardData.setData) {
      return clipboardData.setData("Text", text);
    }

    // all other modern
    let target = document.createElement("textarea");
    target.style.position = "absolute";
    target.style.left = "-9999px";
    target.style.top = "0";
    target.textContent = text;
    document.body.appendChild(target);
    target.focus();
    target.setSelectionRange(0, target.value.length);

    // copy the selection of fall back to prompt
    try {
      document.execCommand("copy");
      target.remove();
    } catch (e) {
      console.log("Can't copy string on this browser. Try to use Chrome, Firefox or Opera.")
      window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
    }
  }

  copyLabel(event, value) {
    let label = this.props.jsonConfig.label
    event.preventDefault()
    let result = []
    this.state.tags.forEach(element => {
      result.push(element[label])
    });
    this.copyToClipboard(result.join(this.props.copySplitFlag))

  }


  renderInputComponent() {
    let { jsonConfig } = this.props
    let triggerStyle = {}
    if (this.props.inputWidth) {
      triggerStyle.width = this.props.inputWidth
    }
    else {
      triggerStyle.width = "100%"
    }
    if (this.props.io == "out") {
      triggerStyle.width = "100%"
      triggerStyle.border = "none"
    }

    return (
      <div className={cn(this.props.className, 'react-dropdown-tree-select', 'input-group')} id={this.componentId} name={this.getName()} required={this.props.required}>
        <Dropdown disabled={this.props.disabled} ref={el => { this.dropdown = el }} onHide={this.onDrowdownHide} >
          <DropdownTrigger className={cx('dropdown-trigger')} style={triggerStyle}
            onMouseOver={this.showClearBtn.bind(this)}
            onMouseOut={this.hideClearBtn.bind(this)}
          >
            <Input
              // inputRef={el => { this.searchInput = el }}
              tags={this.state.tags}
              required="true"
              // placeholderText={this.props.placeholderText}
              // onInputChange={this.onInputChange}
              // onFocus={() => { this.keepDropdownActive = true }}
              // onBlur={() => { this.keepDropdownActive = false }}
              onTagRemove={this.onTagRemove}
              mostVisableItem={this.props.mostVisableItem}
              perItemMaxLength={this.props.perItemMaxLength}
              labellabel={jsonConfig.label}
              renderInput={this.renderInput.bind(this)}
            // copySplitFlag={this.props.copySplitFlag}
            />
            <span className="rainbow Clear rb-clear" style={{ display: `${this.state.isShowClear ? 'block' : 'none'}` }} onClick={() => this.clearAll()}></span>
            <span className="rainbow SingArrowDown"></span>
          </DropdownTrigger>
          <DropdownContent className={cx('dropdown-content')} style={this.props.contentWidth ? { width: this.props.contentWidth } : { "width": "100%" }}>

            {this.state.allNodesHidden
              ? <span className='no-matches'>No matches found</span>
              : (
                <Tree data={this.state.tree}
                  searchModeOn={this.state.searchModeOn}
                  onAction={this.onAction}
                  onCheckboxChange={this.onCheckboxChange}
                  onNodeToggle={this.onNodeToggle} jsonConfig={jsonConfig} />
              )
            }
          </DropdownContent>
        </Dropdown>
      </div>
    )
  }

  showClearBtn() {
    if (this.state.tags.length > 0) {
      this.setState({ isShowClear: true });
    }
  }

  hideClearBtn() {
    this.setState({ isShowClear: false });
  }

  // 删除按钮
  clearAll() {
    let { tags = [] } = this.state;
    tags.map(item => {
      this.onCheckboxChange(null, item.elementId, false);
    })
  }

  renderInput() {
    if (this.props.disabled) {
      return null;
    }
    return (
      <input type='text'
        ref={el => { this.searchInput = el }}
        placeholder={this.props.placeholderText || 'Search'}
        onChange={this.onInputChange.bind(this)}
        onFocus={() => { this.keepDropdownActive = true }}
        onBlur={() => { this.keepDropdownActive = false }}
        onCopy={this.copyLabel.bind(this)} />
    )
  }
}



/**
 * DropdownTreeSelect component prop types
 */
DropdownTreeSelect.propTypes = $.extend({}, UIInput.propTypes, {
  data: PropTypes.array.isRequired,
  placeholderText: PropTypes.string,
  showDropdown: PropTypes.bool,
  className: PropTypes.string,
  onChange: PropTypes.func,
  onCheckboxChange: PropTypes.func,
  // onAction: PropTypes.func,
  // onNodeToggle: PropTypes.func,
  defaultValue: PropTypes.array,
  readOnly: PropTypes.bool,
  treeSearchMode: PropTypes.bool,
  mostVisableItem: PropTypes.number,
  copySplitFlag: PropTypes.string,
  perItemMaxLength: PropTypes.number,
  pinyinForSearch: PropTypes.bool,
  inputWidth: PropTypes.string,
  contentWidth: PropTypes.string,
  noCheckParent: PropTypes.bool,
  propSaveConent: PropTypes.oneOf(["leaf", "tag"]),
  propSaveConentType: PropTypes.oneOf(["id", "node"]),
  jsonConfig: PropTypes.shape({
    id: PropTypes.string,
    children: PropTypes.string,
    label: PropTypes.string
  }),
  disabled: PropTypes.bool
});

/**
 * Get DropdownTreeSelect component default props
 */
DropdownTreeSelect.defaultProps = $.extend({}, UIInput.defaultProps, {
  readOnly: false,
  treeSearchMode: true,
  mostVisableItem: 3,
  copySplitFlag: ",",
  perItemMaxLength: 8,
  defaultValue: [],
  noCheckParent: false,
  propSaveConent: "leaf",
  propSaveConentType: "id",
  pinyinForSearch: false,
  jsonConfig: {
    id: "id",
    children: "children",
    label: "label"
  },
  disabled: false
});