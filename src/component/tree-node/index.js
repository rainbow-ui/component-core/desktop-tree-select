import { Component } from 'rainbowui-desktop-core'
import PropTypes from 'prop-types'
// import isEmpty from 'lodash/isEmpty'
import Action from './action'
import cn from 'classnames/bind'
import styles from '../../css/treenode.css'

const cx = cn.bind(styles)

export default class TreeNode extends Component {
  static propTypes = {
  }

  componentDidMount() {
    let arrLi = $('.root li');
    for (let i = 0; i < arrLi.length; i++) {
      if ($(arrLi[i]).attr('class').indexOf('leaf') != -1) {
        let paddingLeft = $(arrLi[i]).css('padding-left').replace('px','');
        let nextPaddingLeft = 0;
        if ($(arrLi[i]).next() && $(arrLi[i]).next()) {
          if ($(arrLi[i]).next().css('padding-left')) {
            nextPaddingLeft = $(arrLi[i]).next().css('padding-left').replace('px','');
          }
          if (parseInt(nextPaddingLeft) < parseInt(paddingLeft)) {
            $(arrLi[i]).addClass('lastleaf')
          }
        }
        
      }
    }
  }
  render() {
    const { node, onNodeToggle, onCheckboxChange, onAction, labellabel } = this.props;
    const actions = node.actions || []
    const isLeaf = node.hasChild && node.hasChild == 1 ? false : _.isEmpty(node._children);
    const liCx = cx('node', { leaf: isLeaf, tree: !isLeaf, hide: node.hide }, node.className)
    // const toggleCx = cx('toggle', { expanded: !isLeaf && node.expanded, collapsed: !isLeaf && !node.expanded })
    const toggleCx = cx('toggle rainbow SingArrowDown', { 'rb-minus': isLeaf || !isLeaf && node.expanded, 'rb-plus': !isLeaf && !node.expanded })
    const nodeCx = cx('node-label', { 'highlight': node.highlight })
    const pinyinStr = node["highLightText"] == 0 ? 0 : (node["highLightText"] || -1)

    return (
      <li className={liCx} style={{ paddingLeft: `${node._depth * 20}px` }}>
        <i className={toggleCx} onClick={() => onNodeToggle(node)} />
        <label title={node.title || node[labellabel]}>
          <input type='checkbox'
            name={node._id}
            className='checkbox-item'
            checked={node.checked}
            onChange={e => onCheckboxChange(e, node._id, e.target.checked)}
            value={node.value} />
          <span className={nodeCx} pinyin={pinyinStr}>{node[labellabel]}</span>
        </label>
        {actions.map((a, idx) => <Action key={`action-${idx}`} {...a} actionData={{ action: a.id, node }} onAction={onAction} />)}
      </li>
    )
  }
}




/**
 * TreeNode component prop types
 */
TreeNode.propTypes = $.extend({}, Component.propTypes, {
  node: PropTypes.shape({
    _id: PropTypes.string,
    _depth: PropTypes.number,
    _children: PropTypes.array,
    actions: PropTypes.array,
    className: PropTypes.string,
    title: PropTypes.string,
    label: PropTypes.string.isRequired,
    checked: PropTypes.bool,
    expanded: PropTypes.bool
  }).isRequired,
  onNodeToggle: PropTypes.func,
  onAction: PropTypes.func,
  onCheckboxChange: PropTypes.func,
  labellabel: PropTypes.string

});

/**
 * Get TreeNode component default props
 */
TreeNode.defaultProps = $.extend({}, Component.defaultProps, {
});