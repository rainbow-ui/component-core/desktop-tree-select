/*!
 * React Dropdown Tree Select
 * A lightweight, fast and highly customizable tree select component.
 * Hrusikesh Panda <hrusikesh.panda@dowjones.com>
 * Copyright (c) 2017 Dow Jones, Inc. <support@dowjones.com> (http://dowjones.com)
 * license MIT
 * see https://github.com/dowjones/react-dropdow-tree-select
 */
import { UIInput, UISwitch, Param, UIMessageHelper, UIDataTable, UIColumn, UIText, UICheckbox, OnChangeEvent, UIChoose } from 'rainbowui-desktop-core'
import PropTypes, { instanceOf } from 'prop-types'
import cn from 'classnames/bind'
import Dropdown from './Dropdown'
import DropdownTrigger from './DropdownTrigger'
import DropdownContent from './DropdownContent'
import { Util, ObjectUtil } from 'rainbow-desktop-tools';
import { TableInput } from './input'
// import cloneDeep from 'lodash/cloneDeep'
import styles from '../css/index.css'
import { SelectUtil } from './select-utils.js'

const cx = cn.bind(styles)

export default class InputTableSelect extends UIInput {


    constructor(props) {
        super(props)

        let inputLi = 'inputClass'
        if (props.immediatelyShowTag) {
            inputLi = null
        }

        this.jsonConfig = props.jsonConfig

        this.state = {
            dropdownActive: this.props.showDropdown || false,
            tableData: [],
            realData: [],
            defaultFlag: 'N',
            checkColCode: 'dataTableCheckColumn',
            textForSearch: "tableOriginalTextForSearch",
            pinyinForSearch: "tableOriginalPinyinForSearch",
            inputLiClass: inputLi,
            allNodesHidden: false,
            switchState: { switchValue: '' },
            removeTags: [],

            chooseValueList: [props.allText, props.selectedText],
            chooseValue: props.allText
        }

        if (this.props.io == "out") {
            this.props.readOnly = true
        }

        this.onInputChange = this.onInputChange.bind(this)
        // this.onDrowdownHide = this.onDrowdownHide.bind(this)
    }

    componentDidMount() {
        if (this.state.tags) {
            SelectUtil.setSelectComponentValue(this.state.tags.length, this.componentId);
            if (this.props.io != "out") {
                this.initValidator();
            }
        }
    }

    componentWillMount() {

        super.componentWillMount();
        if (!this.props.data) {
            this.props.data = []
        }
        this.configAndDataInti(this.props)
        if (Util.parseBool(this.props.readOnly)) {

            this.allBoxChecked = null
            this.onCheckRow = null
            this.onTagRemove = null
        }
        else {
            this.allBoxChecked = this.allBoxChecked.bind(this)
            this.onCheckRow = this.onCheckRow.bind(this)
            this.onTagRemove = this.onTagRemove.bind(this)
        }
    }

    configAndDataInti(props) {

        if (this.props.data) {
            this.state.allNodesHidden = true
        } else {
            if (this.props.data.length == 0) {
                this.state.allNodesHidden = true
            }
        }

        let msg = ""
        let checkIds = []
        this.state.tableData = props.data
        this.jsonConfig = props.jsonConfig
        this.state.tableData = _.cloneDeep(this.state.tableData); //$.extend([],this.state.tableData,true); // ObjectUtil.clone(this.props.data); //_.cloneDeep(this.props.data);
        if (this.state.tableData == []) {
            this.state.tableData = [];
        }


        let searchColumns = []

        this.jsonConfig.columns.forEach(element => {
            if (element.searchAble) {
                searchColumns.push(element["colCode"])
            }
        });

        let modelArray = [];
        if (props.model && props.property && props.model[props.property]) {
            modelArray = props.model[props.property];
        }
        // mark
        _.forEach(this.state.tableData, (element) => {
            if (!modelArray.includes(element[this.jsonConfig.idCol])) {
                element[this.state.checkColCode] = 'N'
            }
            else {
                // if (element[this.jsonConfig.dataCheckCode]) {
                // if (element[this.jsonConfig.dataCheckCode] == 'Y') {
                SelectUtil.setSelectComponentValue(1, this.componentId);
                // }
                // }
                // element[this.state.checkColCode] = element[this.jsonConfig.dataCheckCode]
                element[this.state.checkColCode] = 'Y'
            }
            let matchOriginTextArr = []
            let matchPinYinTextArr = []

            searchColumns.forEach(colId => {
                matchOriginTextArr.push(element[colId])
                if (this.props.pinyinForSearch) {
                    if (makePy) {
                        matchPinYinTextArr.push(makePy(element[colId] + '').join('|').toLowerCase()) //todo:杩欓噷鍋氭嫾闊宠浆鎹?
                    }
                }
            });
            element[this.state.textForSearch] = matchOriginTextArr.join('|')
            element[this.state.pinyinForSearch] = matchPinYinTextArr.join('|')
            element["tagClassName"] = this.state.inputLiClass;
            // element['containerId'] = this.componentId;
        });
        this.setState({ realData: _.cloneDeep(this.state.tableData) })
        const tags = this.getTags()
        this.setState({ tags })

        if (msg) {
            UIMessageHelper.warning(msg)
        }
    }

    // 判断全选状态
    judgeCheckAllState(obj, code = this.state.checkColCode) {
        let counter = 0;
        obj.map(item => {
            item[code] === 'Y' && counter++;
        });
        counter === obj.length ? this.setState({ defaultFlag: 'Y' }) : this.setState({ defaultFlag: 'N' });
    }

    getTags() {
        let code = this.state.checkColCode
        let result = []

        _.forEach(this.state.tableData, (element) => {
            if (element[code] == 'Y') {
                result.push(element)
            }
        });
        if (result.length == 0) {
            this.state.defaultFlag = 'N'
            if (this.props.model && this.props.property) {
                this.props.model[this.props.property] = []
            }
        }
        else {
            if (this.props.model && this.props.property) {
                this.props.model[this.props.property] = []
                result.forEach(element => {
                    if (this.jsonConfig.idCol) {
                        let ele = element[this.jsonConfig.idCol];
                        ele && this.props.model[this.props.property].push(ele);
                    } else {
                        this.props.model[this.props.property].push(element);
                    }
                });
            }
        }

        return result;
    }


    componentDidUpdate() {
    }

    componentWillReceiveProps(nextProps) {

        if (nextProps.jsonConfig) {
            this.jsonConfig = nextProps.jsonConfig
        }

        let bindProperty = nextProps.property
        let msg = ""
        if (nextProps.io == "out") {
            nextProps.readOnly = true
        }


        if (nextProps.model && nextProps.property && (nextProps.data || nextProps.codeTableName)) {
            if ((_.isEqual(this.props.data, nextProps.data) && _.isEqual(this.props.codeTableName, nextProps.codeTableName)) &&
                _.isEqual(this.props.model, nextProps.model) &&
                _.isEqual(this.props.property, nextProps.property)) {
                return
            }
        }

        if (!nextProps.data) {
            nextProps.data = []
        }
        this.configAndDataInti(nextProps)
        if (Util.parseBool(nextProps.readOnly)) {

            this.allBoxChecked = function () { }
            this.onCheckRow = function () { }
            this.onTagRemove = function () { }
        }
        else {
            this.allBoxChecked = this.allBoxChecked.bind(this)
            this.onCheckRow = this.onCheckRow.bind(this)
            this.onTagRemove = this.onTagRemove.bind(this)
        }
    }

    onDrowdownHide() {
        // needed when you click an item in tree and then click back in the input box.
        // react-simple-dropdown behavior is toggle since its single select only
        // but we want the drawer to remain open in this scenario as we support multi select
        if ($('#' + this.componentId).parents('td') && $('#' + this.componentId).parents('td').length > 0) {
            $('#for-input-' + this.componentId).css({ position: 'relative', left: '0px', marginTop: '0px' });
        }
        if (this.keepDropdownActive) {
            this.dropdown.show()
        }
    }

    contentOnShow() {
        if ($('#' + this.componentId).parents('td') && $('#' + this.componentId).parents('td').length > 0) {
            let l = $('#for-input-' + this.componentId).position().left;
            let w = $('#' + this.componentId).parents('td').attr('realwidth');
            let curObjH = $('#for-input-' + this.componentId).height() / 2;
            $('#for-input-' + this.componentId).css({ position: 'absolute', left: l + 'px', marginTop: '-' + curObjH + 'px' });
        }
        let value = $("#" + this.componentId + "-searchInput").val()
        if (value) {

            if (value.length > 0) {
                this.filterTable(value.split(' '))
            }
            else {
                this.setState({
                    tableData: this.state.tableData,
                    switchState: { switchValue: 'N' }
                })
            }
        }
        $("#" + this.componentId + "-searchInput").focus()
    }

    onInputChange(value) {
        this.setState({ isShowSearchTable: true });
        if (value.length > 0) {
            this.filterTable(value.split(' '))
        } else {
            this.setState({
                searchTableData: this.state.tableData
            })
        }
        // else {
        //     this.setState({
        //         tableData: this.state.realData,
        //         allNodesHidden: false
        //     })
        // }
    }

    filterTable(matchArr) {
        let filterTableResult = []

        this.state.tableData.forEach(element => {
            let isMatch = 0
            let isPinyinMatch = 0
            matchArr.forEach(matchStr => {
                isMatch |= element[this.state.textForSearch].toLowerCase().search(matchStr.toLowerCase())
                if (this.props.pinyinForSearch) {
                    isPinyinMatch |= element[this.state.pinyinForSearch].toLowerCase().search(matchStr.toLowerCase())
                } else {
                    isPinyinMatch = -1
                }
            });
            if (isMatch >= 0 || isPinyinMatch >= 0) {
                filterTableResult.push(element)
            }
        });
        if (filterTableResult.length == 0) {
            this.setState({
                // allNodesHidden: true,
                // tableData: this.state.realData,
                // switchState: { switchValue: 'N' }
                searchTableData: []
            })
        }
        else {
            this.setState({
                // tableData: filterTableResult,
                // allNodesHidden: false,
                // switchState: { switchValue: 'N' }
                searchTableData: filterTableResult
            })
        }
    }

    onTagRemove(id, tag, pContainerId) {
        /* 修改组件每次点击X按钮都删除第一个的问题 */
        let cur = this.state.tags.find(c => c.dataIndex == tag.dataIndex);
        cur[this.state.checkColCode] = 'N';
        this.onCheckRow();
        this.state.removeTags.push({ componentId: this.componentId, tag: tag });
    }

    copyToClipboard(text) {
        // IE specific
        if (window.clipboardData && window.clipboardData.setData) {
            return clipboardData.setData("Text", text);
        }

        // all other modern
        let target = document.createElement("textarea");
        target.style.position = "absolute";
        target.style.left = "-9999px";
        target.style.top = "0";
        target.textContent = text;
        document.body.appendChild(target);
        target.focus();
        target.setSelectionRange(0, target.value.length);

        // copy the selection of fall back to prompt
        try {
            document.execCommand("copy");
            target.remove();
        } catch (e) {
            console.log("Can't copy string on this browser. Try to use Chrome, Firefox or Opera.")
            window.prompt("Copy to clipboard: Ctrl+C, Enter", text);
        }
    }

    copyLabel(event, value) {
        let label = this.props.jsonConfig.label
        event.preventDefault()
        let result = []
        this.state.tags.forEach(element => {
            result.push(element[label])
        });
        this.copyToClipboard(result.join(this.props.copySplitFlag))

    }

    generateColumnData(jsonConfig) {
        let columnResult = [];
        // const idColumn = jsonConfig["idCol"]
        // const diaplayByIndex = jsonConfig["diaplayByIndex"]
        let configColumns = jsonConfig["columns"]
        let self = this
        let columns = []

        if (configColumns) {
            columns = configColumns.sort(this.objectSort('index', false))
        }
        //判断是不是自己有checkbox列，如果有就不管了，没有就加一下
        columnResult.push(
            <UIColumn width="5%" headerTitle={this.getCheckBox.bind(this)()} render={
                (data) => {
                    return (
                        <div className={`${this.props.singleSelect ? 'single-select' : ''}`}>
                            <UICheckbox model={data} single="true" property={self.state.checkColCode} onChange={self.onCheckRow} enabled={!this.props.readOnly}
                            />
                        </div>
                    )
                }
            } />

        )

        columns.forEach(element => {
            let headerTitle = element.colLabel
            let columnCode = element.colCode
            let columnWidth = element.columnWidth
            columnResult.push(
                <UIColumn headerTitle={headerTitle} value={columnCode} width={columnWidth}>
                    <UIText io="out" />
                </UIColumn>
            )

        });
        return columnResult
    }

    onCheckRow(e) {
        let _self = this;
        if (e && this.props.singleSelect) {
            let checkSign = this.state.checkColCode
            let idc = this.props.jsonConfig.idCol
            _.forEach(this.state.tableData, (element) => {
                if (e.component.props.model[idc] != element[idc]) {
                    element[checkSign] = 'N';
                }
            });
        }
        if (this.state.isShowCheckedTable) {
            this.judgeCheckAllState(this.state.checkedTableData);
        } else if (this.state.isShowSearchTable) {
            this.judgeCheckAllState(this.state.searchTableData);
        } else {
            this.judgeCheckAllState(this.state.tableData);
        }
        const tags = this.getTags();
        SelectUtil.setSelectComponentValue(tags.length, this.componentId)
        this.setState({ defaultFlag: this.state.defaultFlag, tags: tags })
        if (_self.props.onChange) {
            _self.onChangeCallback(_self);
        }
    }

    getCheckBox() {
        return (
            !this.props.singleSelect ?
                <UICheckbox single="true" model={this.state} property="defaultFlag" onChange={this.allBoxChecked} enabled={!this.props.readOnly} />
                : <span></span>
        )
    }

    allBoxChecked(event) {
        let _self = this;
        this.state.defaultFlag = event.newValue;
        let checkSign = this.state.checkColCode
        _.forEach(this.state.tableData, (element) => {
            element[checkSign] = event.newValue;
        });
        const tags = this.getTags()
        SelectUtil.setSelectComponentValue(tags.length, this.componentId)
        this.setState({ tableData: this.state.tableData, tags: tags })
        if (_self.props.onChange) {
            _self.onChangeCallback(_self);
        }
    }

    onChooseChanged() {
        let { chooseValue, chooseValueList } = this.state;
        if (chooseValue === chooseValueList[0]) {
            // 展示全部
            this.setState({ isShowCheckedTable: false, isShowSearchTable: false });
            this.judgeCheckAllState(this.state.tableData);
        } else {
            // 展示选中
            let temp = [];
            let { tableData = [], checkColCode } = this.state;
            temp = tableData.filter(item => {
                if (item[checkColCode] === 'Y') {
                    return temp;
                }
            })

            this.setState({
                isShowCheckedTable: true,
                checkedTableData: temp,
                defaultFlag: temp.length > 0 ? 'Y' : 'N'
            })
        }
    }

    onSwitchChanged(event) {

        let tempTableData = [];
        let _self = this;
        if (this.state.switchState.switchValue == 'Y') {
            _.forEach(this.state.tableData, (element, index) => {
                if (element[this.state.checkColCode] == 'Y') {
                    tempTableData.push(element);
                    _self.state.realData[index][_self.state.checkColCode] = 'Y'
                }
            });
            this.setState({
                tableData: tempTableData,
                allNodesHidden: tempTableData.length == 0
            })
        }
        else {
            this.setState({
                tableData: this.state.realData,// this.state.tableData,
                allNodesHidden: false// this.state.tableData.length == 0
            })
            // }
        }

    }


    objectSort(property, desc) {
        //降序排列 
        if (desc) {
            return function (a, b) {
                return (a[property] > b[property]) ? -1 : (a[property] < b[property]) ? 1 : 0;
            }
        }
        return function (a, b) {
            return (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        }
    }


    renderInputComponent() {
        let { jsonConfig } = this.props
        let triggerStyle = {}
        if (this.props.inputWidth) {
            triggerStyle.width = this.props.inputWidth
        }
        else {
            triggerStyle.width = "100%"
        }
        if (this.props.io == "out") {
            triggerStyle.width = "100%"
            triggerStyle.border = "none"
        }
        return (
            <div className={cn(this.props.className, 'react-dropdown-tree-select', 'input-group')} id={this.componentId} name={this.getName()} required={this.props.required}>
                <Dropdown disabled={this.props.disabled} ref={el => { this.dropdown = el }} onHide={this.onDrowdownHide.bind(this)} onShow={this.contentOnShow.bind(this)}>
                    <DropdownTrigger className={cx('dropdown-trigger')} style={triggerStyle}
                        onMouseOver={this.showClearBtn.bind(this)}
                        onMouseOut={this.hideClearBtn.bind(this)}
                    >
                        <TableInput
                            inputRef={el => { this.searchInput = el }}
                            key={'dropdown' + this.componentId}
                            tags={this.state.tags} //{$.extend([],this.state.tags,true)} // {this.state.tags}
                            required="true"
                            placeholderText={this.props.placeholderText}
                            onInputChange={this.onInputChange}
                            onFocus={() => {
                                this.setState({ isShowCheckedTable: false, isShowSearchTable: false })
                                this.keepDropdownActive = true;
                            }}
                            onBlur={() => { this.inputOnBlur() }}
                            onTagRemove={this.onTagRemove}
                            mostVisableItem={this.props.mostVisableItem}
                            perItemMaxLength={this.props.perItemMaxLength}
                            tableLabel={jsonConfig.displayCol}
                            tableId={jsonConfig.idCol}
                            onCopy={this.copyLabel.bind(this)}
                            pContainerId={this.componentId}
                            disabled={this.props.disabled}
                        />
                        {this.props.showDeleteIcon ?
                            <span className="rainbow Clear rb-clear" style={{ display: `${this.state.isShowClear ? 'block' : 'none'}` }} onClick={() => this.clearAll()}></span> : null}

                        <span className="rainbow SingArrowDown"></span>
                    </DropdownTrigger>
                    <DropdownContent className={cx('dropdown-content')} style={this.props.contentWidth ? { width: this.props.contentWidth } : { "width": "100%" }}>
                        <div>
                            {/* <UISwitch model={this.state.switchState} property="switchValue" onText={i18n.Selected} offText={i18n.All} onChange={this.onSwitchChanged.bind(this)} /> */}
                            <UIChoose value={this.state.chooseValueList} multiSelect="false" model={this.state} property="chooseValue" textAlign="right" onClick={this.onChooseChanged.bind(this)} />
                            {this.state.isShowCheckedTable
                                ?
                                (
                                    <UIDataTable id="sort" value={this.state.checkedTableData} showPaginationNum={false} pageMaxNumber={3} pageable={this.props.pageable} pageSize="5" indexable="false" >
                                        {this.generateColumnData(this.jsonConfig)}
                                    </UIDataTable>
                                ) : this.state.isShowSearchTable ?
                                    (
                                        <UIDataTable id="sort" value={this.state.searchTableData} showPaginationNum={false} pageMaxNumber={3} pageable={this.props.pageable} pageSize="5" indexable="false" >
                                            {this.generateColumnData(this.jsonConfig)}
                                        </UIDataTable>
                                    )
                                    : (
                                        <UIDataTable id="sort" value={this.state.tableData} showPaginationNum={false} pageMaxNumber={3} pageable={this.props.pageable} pageSize="5" indexable="false" >
                                            {this.generateColumnData(this.jsonConfig)}
                                        </UIDataTable>
                                    )
                            }
                        </div>
                    </DropdownContent>
                </Dropdown>
            </div>
        )
    }
    showClearBtn() {
        if (this.state.tags.length > 0) {
            this.setState({ isShowClear: true });
        }
    }

    hideClearBtn() {
        this.setState({ isShowClear: false });
    }
    clearAll() {
        // 清空内容
        let { checkColCode, tableData } = this.state;
        tableData = tableData.map(item => {
            item[checkColCode] = 'N';
            return item;
        });
        this.setState({ tableData, tags: [], defaultFlag: false });
    }
    // 输入框失去焦点时，清空输入内容
    inputOnBlur() {
        this.keepDropdownActive = false;
        document.getElementById(this.componentId + '-searchInput').value = '';
    }
}



/**
 * InputTableSelect component prop types
 */
InputTableSelect.propTypes = $.extend({}, UIInput.propTypes, {
    data: PropTypes.array,
    placeholderText: PropTypes.string,
    showDropdown: PropTypes.bool,
    className: PropTypes.string,
    onChange: PropTypes.func,
    defaultValue: PropTypes.array,
    readOnly: PropTypes.bool,
    mostVisableItem: PropTypes.number,
    copySplitFlag: PropTypes.string,
    perItemMaxLength: PropTypes.number,
    inputWidth: PropTypes.string,
    contentWidth: PropTypes.string,
    immediatelyShowTag: PropTypes.bool,
    pinyinForSearch: PropTypes.bool,
    codeTableName: PropTypes.string,
    conditionMap: PropTypes.object,
    pageable: PropTypes.bool,
    singleSelect: PropTypes.bool,
    overflowMarginTop: PropTypes.string,
    jsonConfig: PropTypes.shape({
        dataType: PropTypes.array,
        idCol: PropTypes.string,
        diaplayByIndex: PropTypes.bool,
        dataCheckCode: PropTypes.string,
        onCheck: PropTypes.func,
        columns: PropTypes.shape(
            [{
                colCode: PropTypes.string,
                colLabel: PropTypes.string,
                searchAble: PropTypes.bool,
                index: PropTypes.number,
                sortAble: PropTypes.bool,
            }]
        )
    }).isRequired,
    allText: PropTypes.string,
    selectedText: PropTypes.string,
    disabled: PropTypes.bool,
    showDeleteIcon: PropTypes.bool
})

/**
 * Get InputTableSelect component default props
 */
InputTableSelect.defaultProps = $.extend({}, UIInput.defaultProps, {
    readOnly: false,
    singleSelect: false,
    mostVisableItem: 3,
    copySplitFlag: ",",
    perItemMaxLength: 8,
    defaultValue: [],
    noCheckParent: false,
    immediatelyShowTag: false,
    pinyinForSearch: true,
    componentType: 'InputTableSelect',
    pageable: true,
    overflowMarginTop: '-14px',
    allText: 'All',
    selectedText: 'Selected',
    disabled: false,
    showDeleteIcon: true
});