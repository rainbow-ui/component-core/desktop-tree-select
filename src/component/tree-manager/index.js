// import isEmpty from 'lodash/isEmpty'
// import cloneDeep from 'lodash/cloneDeep'
import flattenTree from './flatten-tree'

export default class TreeManager {
  constructor(tree, config, pinyinForSearch) {
    this._src = tree
    this.jsonConfig = config
    this.tree = flattenTree(_.cloneDeep(tree), config)
    this.tree.forEach(node => { this.setInitialCheckState(node, config, pinyinForSearch) })
    this.searchMaps = new Map()
    this.nodeMatchIndexObj = {};
  }

  getNodeById(id) {
    return this.tree.get(id)
  }

  getNodeByIds(ids) {
    let nodeArr = []
    ids.forEach((id) => {
      let tempNode = this.tree.get(id);
      if (tempNode == undefined) {
        tempNode = { errState: "ERR", errId: id }
      }
      nodeArr.push(tempNode)
    })
    return nodeArr
  }


  getNodeIdsByIdRecursive(ids, resultArr, nodeType) {
    for (let idIndex in ids) {
      let node = this.tree.get(ids[idIndex])
      if (!_.isEmpty(node._children)) {
        this.getNodeIdsByIdRecursive(node._children, resultArr)
      }
      else {
        if (nodeType) {
          resultArr.push(node)
        }
        else {
          resultArr.push(ids[idIndex])
        }
      }
    }
  }


  getMatchesForCheckStatus(checkedValue) {
    let tempObj = {}
    let self = this
    const matches = []
    this.tree.forEach(node => {
      if (node.checked == checkedValue) {
        if (!tempObj[node._id]) {
          self.getNodesALlLeaf(node, matches, tempObj)
          self.getNodesAllParent(node, matches, tempObj)

        }
      }
    })
    return matches
  }

  getMatches(searchTerm, treeSearchMode, labellabel, pinyinMatch) {
    let tempObj = {}
    let self = this
    if (this.searchMaps.has(searchTerm)) {
      return this.searchMaps.get(searchTerm)
    }

    let proximity = -1
    let closestMatch = searchTerm
    this.searchMaps.forEach((m, key) => {
      if (searchTerm.startsWith(key) && key.length > proximity) {
        proximity = key.length
        closestMatch = key
      }
    })

    const matches = []

    if (closestMatch !== searchTerm) {
      const superMatches = this.searchMaps.get(closestMatch)
      if (treeSearchMode) {
        superMatches.forEach(key => {
          const node = this.getNodeById(key)
          let indexMatch = String(node[labellabel]).toLowerCase().indexOf(searchTerm);
          if (indexMatch >= 0) {
            this.nodeMatchIndexObj[node._id + "|" + searchTerm] = indexMatch
            if (!tempObj[node._id]) {
              self.getNodesALlLeaf(node, matches, tempObj)
              self.getNodesAllParent(node, matches, tempObj)
            }
          }
          else {
            if (pinyinMatch) {
              // if (node["treeNodePinyin"].toLowerCase().indexOf(searchTerm) >= 0) {
              //   if (!tempObj[node._id]) {
              //     self.getNodesALlLeaf(node, matches, tempObj)
              //     self.getNodesAllParent(node, matches, tempObj)
              //   }
              // }
              let offset = node["treeNodePinyin"].doMatch(searchTerm)
              this.nodeMatchIndexObj[node._id + "|" + searchTerm] = offset
              if (offset > -1) {
                if (!tempObj[node._id]) {
                  self.getNodesALlLeaf(node, matches, tempObj)
                  self.getNodesAllParent(node, matches, tempObj)
                }
              }
            }
          }
        })
      }
      else {
        superMatches.forEach(key => {
          const node = this.getNodeById(key)
          let indexMatch = node[labellabel].toLowerCase().indexOf(searchTerm);
          this.nodeMatchIndexObj[node._id + "|" + searchTerm] = indexMatch
          if (indexMatch >= 0) {
            tempObj[node._id] = true
            matches.push(node._id)
          }
          else {
            if (pinyinMatch) {
              // if (node["treeNodePinyin"].toLowerCase().indexOf(searchTerm) >= 0) {
              //   if (!tempObj[node._id]) {
              //     matches.push(node._id)
              //   }
              // }
              let offset = node["treeNodePinyin"].doMatch(searchTerm)
              this.nodeMatchIndexObj[node._id + "|" + searchTerm] = offset
              if (offset > 0) {
                if (!tempObj[node._id]) {
                  matches.push(node._id)
                }
              }
            }
          }
        })
      }
    } else {

      if (treeSearchMode) {
        this.tree.forEach(node => {
          let indexMatch = String(node[labellabel]).toLowerCase().indexOf(searchTerm);
          if (indexMatch < 0) {
            if (pinyinMatch) {
              // indexMatch = node["treeNodePinyin"].toLowerCase().indexOf(searchTerm)
              indexMatch = node["treeNodePinyin"].doMatch(searchTerm)
            }
          }
          this.nodeMatchIndexObj[node._id + "|" + searchTerm] = indexMatch
          if (indexMatch > -1) {
            // if (searchTerm.length > 0) {
            //   this.getHighlightLabel(node[labellabel], searchTerm, node._id)
            // }
            if (!tempObj[node._id]) {
              self.getNodesALlLeaf(node, matches, tempObj)
              self.getNodesAllParent(node, matches, tempObj)

            }
          }
        })
      } else {
        this.tree.forEach(node => {
          let indexMatch = node[labellabel].toLowerCase().indexOf(searchTerm) < 0;
          if (indexMatch < 0) {
            if (pinyinMatch) {
              // indexMatch = node["treeNodePinyin"].toLowerCase().indexOf(searchTerm)
              indexMatch = node["treeNodePinyin"].doMatch(searchTerm)
            }
          }

          this.nodeMatchIndexObj[node._id + "|" + searchTerm] = indexMatch
          if (indexMatch > -1) {
            matches.push(node._id)
          }
        })
      }
    }

    this.searchMaps.set(searchTerm, matches)
    return matches
  }


  // getHighlightLabel(label, searchTerm, id) {
  //   let preStr = label.substr(0, label.indexOf(searchTerm))
  //   let nextStr = label.substr(label.indexOf(searchTerm) + searchTerm.length)
  //   let highlightLabel = '<span class="highlight">' + searchTerm + '</span>'
  //   $('.node input[name="' + id + '"]').next().html(preStr + highlightLabel + nextStr)
  // }


  //获取所有父节点
  getNodesAllParent(node, result, tempObj) {
    if (node._parent) {
      let pNode = this.getNodeById(node._parent)
      if (!tempObj[pNode._id]) {
        result.push(pNode._id)
        tempObj[pNode._id] = true
        this.getNodesAllParent(pNode, result, tempObj)
      }
    }
  }

  //获取所有子节点
  getNodesALlLeaf(node, result, tempObj) {
    result.push(node._id)
    tempObj[node._id] = true
    if (!_.isEmpty(node._children)) {
      let tempArr = this.getNodeByIds(node._children)
      for (let cIndex in tempArr) {
        let tempNode = tempArr[cIndex]
        if (tempNode.errState) {
          continue;
        }
        this.getNodesALlLeaf(tempNode, result, tempObj)
      }
    }
  }


  //查找所有选中的节点
  filterTreeChecked(checkedValue) {

    const matches = this.getMatchesForCheckStatus(checkedValue)
    this.tree.forEach(node => {
      node.hide = true
    })

    let blankInput = false
    if (matches.length == this.tree.size) {
      blankInput = true
    }

    matches.forEach(m => {
      let node = this.getNodeById(m)
      node.hide = false
      if (!blankInput) {
        if (!_.isEmpty(node._children)) {
          node.expanded = true
        }
      }
      else {
        if (!node._parent) {
          node.expanded = true
        }
        else {
          node.expanded = false
        }
      }
    })
    const allNodesHidden = matches.length === 0
    return { allNodesHidden, tree: this.tree }
  }
  filterTree(searchTerm, treeSearchMode, labellabel, pinyinMatch) {

    let matches = this.getMatches(searchTerm.toLowerCase(), treeSearchMode, labellabel, pinyinMatch);
    if (matches == null) {
      matches = []
    }
    if (searchTerm.length == 0) {
      this.tree.forEach(node => {
        node["highLightText"] = -1
        node.hide = false
      })
    }
    else {

      this.tree.forEach(node => {
        let nodeMatchStatus = this.nodeMatchIndexObj[node._id + '|' + searchTerm.toLowerCase()]
        node["highLightText"] = nodeMatchStatus != -1 ? nodeMatchStatus : -1
        node.hide = true
      })


      let blankInput = false
      if (matches.length == this.tree.size || searchTerm.length == 0) {
        blankInput = true
      }

      matches.forEach(m => {
        const node = this.getNodeById(m)

        node.hide = false
        if (!blankInput) {
          if (!_.isEmpty(node._children)) {
            node.expanded = true
          }
        }
        else {
          if (!node._parent) {
            node.expanded = true
          }
          else {
            node.expanded = false
          }
        }
      })

    }

    let allNodesHidden = matches.length === 0

    if (searchTerm.length == 0) {
      allNodesHidden = false
    }

    return { allNodesHidden, tree: this.tree }
  }

  restoreNodes() {
    this.tree.forEach(node => {
      node.hide = false
    })

    return this.tree
  }

  /**
  * If the node didn't specify anything on its own
  * figure out the initial state based on parent selections
  * @param {object} node [description]
  */
  setInitialCheckState(node, config, pinyinForSearch) {
    let nodeLabel;
    if (config) {
      nodeLabel = config.label;
    }
    if (pinyinForSearch) {
      if (makePyCCIC) {
        if (nodeLabel) {
          if (node[nodeLabel]) {
            // node["treeNodePinyin"] = makePy(node[nodeLabel] + '').join('|').toLowerCase();
            node["treeNodePinyin"] = makePyCCIC(node[nodeLabel] + '');
          }
        }
      }
    }
    if (node.checked === undefined) {
      node.checked = this.getNodeCheckedState(node)
    }
  }

  /**
   * Figure out the check state based on parent selections.
   * @param  {[type]} node    [description]
   * @param  {[type]} tree    [description]
   * @return {[type]}         [description]
   */
  getNodeCheckedState(node) {
    let parentCheckState = false
    let parent = node._parent
    while (parent && !parentCheckState) {
      const parentNode = this.getNodeById(parent)
      parentCheckState = parentNode.checked || false
      parent = parentNode._parent
    }

    return parentCheckState
  }

  setNodeCheckedState(id, checked, noLinkage, singleSelect) {
    const node = this.getNodeById(id)
    if (node == undefined) {
      return id + " not found in dataSource"
    }

    if (noLinkage && singleSelect) {

      this.tree.forEach(cnode => {
        cnode.checked = false
      })

      node.checked = checked
      return
    }
    node.checked = checked
    if (!noLinkage) {
      this.toggleChildren(id, checked)

      if (!checked) {
        this.unCheckParents(node)
      }

      if (node._parent) {
        this.checkParent(node._parent)
      }
    }
  }


  //add by guf
  setNodeCheckedStates(ids, checked, noLinkage) {
    let msg = ""
    let errMsg = []
    if (noLinkage) {
      const nodeArr = this.getNodeByIds(ids)

      nodeArr.forEach((node) => {
        if (node.errState) {
          errMsg.push(node.errId)
          return;
        }
        node.checked = checked;
      });

    }
    else {
      const nodeArr = this.getNodeByIds(ids)
      let parentArr = []
      nodeArr.forEach((node) => {
        if (node.errState) {
          errMsg.push(node.errId)
          return;
        }
        if (node._parent) {
          parentArr.push(node._parent)
        }
        // if (_.isEmpty(node._children)) {
        node.checked = checked
        this.toggleChildren(node._id, checked)

        if (!checked) {
          this.unCheckParents(node)
        }
        // }
      })

      let parentArrDistinct = []
      this.getWholeParentArrdistint(parentArr, parentArrDistinct)

      if (parentArrDistinct) {
        this.checkParents(parentArrDistinct)
      }

    }
    msg = errMsg.join(',')
    if (msg) {
      return msg + " not found in dataSource"
    }
  }

  getWholeParentArrdistint(parentArr, result) {
    let parentArrDistinct = Array.from(new Set(parentArr))
    if (parentArrDistinct.length > 0) {
      parentArrDistinct.forEach(ele => {
        result.push(ele)
      })
    }
    const nodeArr = this.getNodeByIds(parentArrDistinct)
    let parentPArr = []
    nodeArr.forEach((node) => {
      if (node.errState) {
        return;
      }

      if (node._parent) {
        parentPArr.push(node._parent)
      }
    })
    if (parentPArr.length > 0) {
      this.getWholeParentArrdistint(parentPArr, result)
    }
  }


  /**
   * Walks up the tree unchecking parent nodes
   * @param  {[type]} node [description]
   * @return {[type]}      [description]
   */
  unCheckParents(node) {
    let parent = node._parent
    while (parent) {
      let next = this.getNodeById(parent)
      next.checked = false
      parent = next._parent
    }
  }


  checkParent(pId) {
    let node = this.getNodeById(pId)
    let cNodes = this.getNodeByIds(node._children)
    let needCheck = true
    for (var cIndex in cNodes) {
      if (cNodes[cIndex].errState) {
        continue;
      }
      if (!cNodes[cIndex].checked) {
        needCheck = false
        break
      }
    }
    node.checked = needCheck
    if (node._parent) {
      this.checkParent(node._parent)
    }
  }

  checkParents(ids) {
    if (ids) {
      for (var index in ids) {
        let node = this.getNodeById(ids[index])
        let cNodes = this.getNodeByIds(node._children)
        let needCheck = true
        for (var cIndex in cNodes) {

          let tempNode = cNodes[cIndex]
          if (tempNode.errState) {
            continue;
          }

          if (!tempNode.checked) {
            needCheck = false
            break
          }
        }
        node.checked = needCheck
      }
    }
  }

  toggleChildren(id, state) {
    const node = this.getNodeById(id)
    node.checked = state
    if (!_.isEmpty(node._children)) {
      node._children.forEach(id => this.toggleChildren(id, state))
    }
  }

  toggleNodeExpandState(id) {
    const node = this.getNodeById(id)
    node.expanded = !node.expanded
    if (!node.expanded) this.collapseChildren(node)
    return this.tree
  }

  collapseChildren(node) {
    node.expanded = false
    if (!_.isEmpty(node._children)) {
      node._children.forEach(c => this.collapseChildren(this.getNodeById(c)))
    }
  }

  getParentNodes(node, propName, pList) {
    if (node._parent) {
      let pNode = this.getNodeById(node._parent)
      if (propName) {
        pList.push(pNode[propName])
      }
      else {
        pList.push(pNode)
      }
      this.getParentNodes(pNode, propName, pList)
    }
  }


  getTags(nodeIds, inputClass, noMarkSubNode, showParentSplitSign) {
    let tags = []
    const visited = {}
    const markSubTreeVisited = (node) => {
      visited[node._id] = true
      if (!_.isEmpty(node._children)) node._children.forEach(c => markSubTreeVisited(this.getNodeById(c)))
    }

    this.tree.forEach((node, key) => {
      if (visited[key]) return

      if (inputClass) {
        node.tagClassName = inputClass
      }
      if (node.checked) {
        // Parent node, so no need to walk children
        let wholeLabel = node[this.jsonConfig["label"]]
        if (showParentSplitSign) {
          let pNodeLabels = []
          this.getParentNodes(node, this.jsonConfig["label"], pNodeLabels)
          if (pNodeLabels.length > 0) {
            let labelResult = []
            for (var i = pNodeLabels.length - 1; i >= 0; i--) {
              labelResult.push(pNodeLabels[i])
            }
            wholeLabel = labelResult.join(showParentSplitSign) + showParentSplitSign + wholeLabel
          }
        }
        node["wholeLabel"] = wholeLabel
        tags.push(node)
        if (nodeIds instanceof Array) {
          nodeIds.push(node._id)
        }
        if (!noMarkSubNode) {
          markSubTreeVisited(node)
        }
      } else {
        visited[key] = true
      }
    })

    if (Array.isArray(tags)) {
      tags = tags.map((item, index) => {
        if (!item.elementId) {
          item.elementId = index + 1;
        }
        return item;
      })
    }
    return tags;
  }
}
