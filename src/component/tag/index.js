import { Component } from 'rainbowui-desktop-core'
import { Util } from 'rainbow-desktop-tools';
import PropTypes from 'prop-types'
import cn from 'classnames/bind'
import styles from '../../css/tag.css'

const cx = cn.bind(styles)

class Button extends Component {
  static propTypes = {
  }

  render() {
    return (
      <button onClick={this.onClick} className={cx('tag-remove')} type='button'>x</button>
    )
  }

  onClick = (e) => {
    // this is needed to stop the drawer from closing
    e.stopPropagation()
    // console.log('tag remove', this.props.tagObj, this.props.id, this.props.pContainerId)
    this.props.onDelete(this.props.id, this.props.tagObj, this.props.pContainerId)
  }
}

export default class Tag extends Component {

  render() {
    const { id, label, onDelete, tagObj, pContainerId } = this.props;
    /* 为tableaelect时显示的选中数据 */
    const { tableOriginalTextForSearch } = tagObj || {};
    if (!Util.parseBool(this.props.readOnly)) {
      return (
        <span className={cx('tag')}>
          {label || tableOriginalTextForSearch}
          <Button id={id} onDelete={onDelete} tagObj={tagObj} pContainerId={pContainerId} />
        </span>
      )
    } else {
      return (
        <span className={cx('tag')} style={{borderRight:'1px solid rgba(0,0,0,.4)', paddingRight:'5px'}}>
          {label}
        </span>
      )
    }
    
  }
}


/**
 * Button component prop types
 */
Button.propTypes = $.extend({}, Component.propTypes, {
  id: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
  tagObj: PropTypes.object
});

/**
 * Get Button component default props
 */
Button.defaultProps = $.extend({}, Component.defaultProps, {
});
/**
 * Tag component prop types
 */
Tag.propTypes = $.extend({}, Component.propTypes, {
  readOnly: PropTypes.bool,
  id: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  onDelete: PropTypes.func,
  tagObj: PropTypes.object
});

/**
 * Get Tag component default props
 */
Tag.defaultProps = $.extend({}, Component.defaultProps, {
  readOnly: false,
});